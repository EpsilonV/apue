#! /bin/sh
##########################################################################
# If not stated otherwise in this file or this component's Licenses.txt
# file the following copyright and licenses apply:
#
# Copyright 2015 RDK Management
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##########################################################################

#######################################################################
#   Copyright [2014] [Cisco Systems, Inc.]
# 
#   Licensed under the Apache License, Version 2.0 (the \"License\");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
# 
#       http://www.apache.org/licenses/LICENSE-2.0
# 
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an \"AS IS\" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#######################################################################


#WEBGUI_SRC=/fss/gw/usr/www/html.tar.bz2
#WEBGUI_DEST=/var/www

#if test -f "$WEBGUI_SRC"
#then
#	if [ ! -d "$WEBGUI_DEST" ]; then
#		/bin/mkdir -p $WEBGUI_DEST
#	fi
#	/bin/tar xjf $WEBGUI_SRC -C $WEBGUI_DEST
#else
#	echo "WEBGUI SRC does not exist!"
#fi
cp -rf /usr/www/hitron /nvram/
#cp /nvram/tftp /tmp/hitron/app/controllers/
#cp /nvram/tcpdump_atom /tmp/hitron/app/controllers/
# start lighttpd
#source /etc/utopia/service.d/log_capture_path.sh
#source /fss/gw/etc/utopia/service.d/log_env_var.sh
REVERT_FLAG="/nvram/webdbg/reverted"
LIGHTTPD_CONF="/nvram/lighttpd.conf"
LIGHTTPD_DEF_CONF="/etc/lighttpd.conf"
XFINITY_FLAG="/nvram/webdbg/xfinitygui"
LIGHTTPD_HITRON_CONF="/nvram/webdbg/lighttpd_hitron.conf"
HITRON="/usr/www/hitron/"

if [ ! -e $XFINITY_FLAG ] ; then
	LIGHTTPD_DEF_CONF=$LIGHTTPD_HITRON_CONF
fi

LIGHTTPD_PID=`pidof lighttpd`
if [ "$LIGHTTPD_PID" != "" ]; then
	/bin/kill $LIGHTTPD_PID
fi

LD_LIBRARY_PATH=/fss/gw/usr/ccsp:$LD_LIBRARY_PATH lighttpd -f $LIGHTTPD_CONF

echo "WEBGUI : Set event"
sysevent set webserver started
